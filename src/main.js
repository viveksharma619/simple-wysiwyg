import Vue from 'vue'
import App from './App.vue'
import wysiwyg from "vue-wysiwyg";



Vue.config.productionTip = false
Vue.use(wysiwyg, {
  hideModules: { "bold": true },

  // you can override icons too, if desired
  // just keep in mind that you may need custom styles in your application to get everything to align
  iconOverrides: { "bold": "<i class='your-custom-icon'></i>" },

  // if the image option is not set, images are inserted as base64
  // image: {
  //   uploadURL: "/api/myEndpoint",
  //   dropzoneOptions: {}
  // },

  // limit content height if you wish. If not set, editor size will grow with content.
  maxHeight: "500px",

  // set to 'true' this will insert plain text without styling when you paste something into the editor.
  forcePlainTextOnPaste: true,

  // specify editor locale, if you don't specify this, the editor will default to english.
  // locale: 'hu'
});

new Vue({
  render: h => h(App),
}).$mount('#app')
